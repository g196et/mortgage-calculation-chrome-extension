$(document).ready(function(){
    $( "#calculate" ).click(function() {
        $.ajax({
            headers: {
                'Api-Key': '728a2644-fefc-43c1-9054-5806c8fda7ef'
            },
            url: "https://api.domclick.ru/calc/api/v2/mortgages/calculate",
            type: "get", //send it through get method
            data: { 
                isIncomeSberbankCard: true, 
                isConfirmIncome: true,
                productId: 3,
                estateCost: $("#estateCost").val(),
                deposit: $("#deposit").val(),
                term: $("#years").val()*12,
                age: 420,
                isInsured: false,
                isMarried: false,
                isHusbandWifeLess35Years: true,
                childrens: 0,
                useOnRegDiscount: true,
                useDeveloperDiscount: true,
                useRealtyDiscount: false,
                kladrId: 7700000000000
            },
            success: function(response) {
                data = response["data"]["calculation"]
                result = "<p>Сумма: " + Math.round(data["MSK"]) + "</p>"
                result += "<p>Ежемесячнй платеж: " + Math.round(data["payment"]) + "</p>"
                result += "<p>Переплата: " + Math.round(data["overpayment"]) + "</p>"
                result += "<p>Необходимый доход: " + Math.round(data["income"]) + "</p>"
                result += "<p>Процентная ставка: " + data["rate"] + "</p>"
                $('#inner').html(result);
            },
            error: function(xhr) {
                console.log(xhr)
            }
        });    
    });
    
});